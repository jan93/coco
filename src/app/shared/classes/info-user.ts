import {Injectable} from '@angular/core';
import {User} from './user';

@Injectable()
export class InfoUser extends User {
  private _nom: string;
  private _prenom: string;
  private _email: string;
  private _metier: string;
  private _age: number;
  private _pays: string;
  private _description: string;
  private _avatar: string;

  get nom(): string {
    return this._nom;
  }

  set nom(value: string) {
    this._nom = value;
  }

  get prenom(): string {
    return this._prenom;
  }

  set prenom(value: string) {
    this._prenom = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get metier(): string {
    return this._metier;
  }

  set metier(value: string) {
    this._metier = value;
  }

  get age(): number {
    return this._age;
  }

  set age(value: number) {
    this._age = value;
  }

  get pays(): string {
    return this._pays;
  }

  set pays(value: string) {
    this._pays = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get avatar(): string {
    return this._avatar;
  }

  set avatar(value: string) {
    this._avatar = value;
  }
}
