export class Bloc {

  private _title: string;
  private _subtitle: string;
  private _purcent: number;
  private _gradientIcon: string;
  private _tag: object;

  constructor() {
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get subtitle(): string {
    return this._subtitle;
  }

  set subtitle(value: string) {
    this._subtitle = value;
  }

  get purcent(): number {
    return this._purcent;
  }

  set purcent(value: number) {
    this._purcent = value;
  }

  get gradientIcon(): string {
    return this._gradientIcon;
  }

  set gradientIcon(value: string) {
    this._gradientIcon = value;
  }

  get tag(): object {
    return this._tag;
  }

  set tag(value: object) {
    this._tag = value;
  }
}
