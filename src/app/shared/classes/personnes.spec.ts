import {PersonnesClass} from './personnes.class';

describe('Personnes', () => {

  let classePersonne;

  beforeEach(() => {
    classePersonne = new PersonnesClass();
    classePersonne.nom = 'jan';
  });

  it('class existe', () => {
    expect(classePersonne).toBeTruthy();
  });

  it('test propriétés', () => {
    expect(classePersonne.nom).toContain('jan');
  });

});
