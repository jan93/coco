import {PersonnesClass} from './personnes.class';

export class PersonnesListClass {

  private personneList: object;


  getPersonneList(): object {
    return this.personneList;
  }

  setPersonneList(value: object): any{
    this.personneList = value;
  }
}
