import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';

@Injectable()
export class LazyDisplayService {

  dataReturn: any = [];

  constructor() { }

  lazyReturnDataDisplayLow(time, data): Observable<any> {
    let i = 0;
    setInterval(() => {
      if (i < data.length) {
        this.dataReturn.push(data[i]);
        i++;
      }
    }, time);
    return of(this.dataReturn);
  }

}
