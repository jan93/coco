import {TestBed} from '@angular/core/testing';

import {LazyDisplayService} from './lazy-display.service';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('LazyDisplayService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [LazyDisplayService],
    schemas: [NO_ERRORS_SCHEMA],
  }));

  it('service bien existant', () => {
    const service: LazyDisplayService = TestBed.get(LazyDisplayService);
    expect(service.lazyReturnDataDisplayLow(100, [1, 2, 3])).toBeTruthy();
  });
});
