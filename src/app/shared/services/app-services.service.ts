import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {interval, Observable, of} from 'rxjs';
import {delay, mergeMap, take} from 'rxjs/operators';

interface Pizza {
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class AppService {


  tableau: Pizza[];

  constructor() {
    this.tableau = [
      {name: 'jean'},
      {name: 'julien'},
      {name: 'belle'},
      {name: 'margerie'},
      {name: 'sylvia'},
      {name: 'eric'},
    ];
  }


  tableauAsync(): Observable<Pizza[]> {
    return of(this.tableau);
  }


}















