import {Component, OnInit} from '@angular/core';
import {AppService} from '../../shared/services/app-services.service';
import {Observable, Observer} from 'rxjs';
import {User} from '../../shared/classes/user';
import {InfoUser} from '../../shared/classes/info-user';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {

  title: string;
  time: any;
  afficheTab: any = [];
  dataRecup: InfoUser;

  constructor(private appService: AppService, private InfoUser: InfoUser) {
    this.dataRecup = this.InfoUser;
    console.log('Class user venant de login', this.dataRecup);
  }

  ngOnInit(): void {
    this.title = 'content composant';
    this.time = new Observable<string>((observer: Observer<string>) => {
      setInterval(() => observer.next(new Date().toString()), 1000);
    });
  }

  actionApi() {
    this.appService.tableauAsync().subscribe(data => {
      this.afficheTab = data;
    });
  }

}
