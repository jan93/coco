import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LayoutComponent} from './layout/layout.component';
import {FooterComponent} from './layout/footer/footer.component';
import {ContentComponent} from './layout/content/content.component';
import {LoginComponent} from './login/login.component';
import {User} from './shared/classes/user';
import {PersonnesClass} from './shared/classes/personnes.class';
import {PersonnesListClass} from './shared/classes/personnesList.class';
import {AppService} from './shared/services/app-services.service';
import {InfoUser} from './shared/classes/info-user';
import {NavbarComponent} from './shared/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    FooterComponent,
    ContentComponent,
    LoginComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    HttpClientModule,

  ],
  providers: [User, InfoUser, PersonnesClass, PersonnesListClass, AppService],
  bootstrap: [AppComponent],
})
export class AppModule {
}
