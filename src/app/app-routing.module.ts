import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './layout/layout.component';
import {LoginComponent} from './login/login.component';
import {ContentComponent} from './layout/content/content.component';


const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'layout',
    component: LayoutComponent,
    children: [
      {path: '', component: ContentComponent},
      {path: 'content', component: ContentComponent},
    ],
  },
  // {
  //   path: 'layout',
  //   canActivate: [AlwaysAuthGuard],
  //   component: LayoutComponent,
  //   children: [
  //     {
  //       path: '',
  //       redirectTo: 'login',
  //       pathMatch: 'prefix',
  //       data: {
  //         breadcrumb: 'main',
  //       },
  //     }, {
  //       path: 'main', component: MainContentComponent,
  //       data: {
  //         breadcrumb: 'main',
  //       },
  //     }, {
  //       path: 'map', component: MapContentComponent,
  //       data: {
  //         breadcrumb: 'map',
  //       },
  //     }, {
  //       path: 'profil', component: ProfilComponent,
  //       data: {
  //         breadcrumb: 'profil',
  //       },
  //     }, {
  //       path: 'temperature', component: TemperatureComponent,
  //       data: {
  //         breadcrumb: 'temperature',
  //       },
  //     }, {
  //       path: 'contact', component: ContactComponent,
  //       data: {
  //         breadcrumb: 'contact',
  //       },
  //     }, {
  //       path: 'tableau',
  //       component: ThalesComponent,
  //       data: {
  //         breadcrumb: 'tableau',
  //       },
  //     }, {
  //       path: 'test-unitaire',
  //       component: TestUnitaireComponent,
  //       data: {
  //         breadcrumb: 'test-unitaire',
  //       },
  //     }, {
  //       path: 'bloc',
  //       component: BlocComponent,
  //       data: {
  //         breadcrumb: 'bloc',
  //       }
  //     }
  //   ],
  // },
  {
    path: '**',
    component: LoginComponent,
  }, {
    path: '',
    component: LoginComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {relativeLinkResolution: 'legacy'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
