import {ComponentFixture, TestBed} from '@angular/core/testing';
import {LoginComponent} from './login.component';
import {InfoUser} from '../shared/classes/info-user';
import {AppModule} from '../app.module';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [LoginComponent],
      providers: [InfoUser]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // it('test InfoUser Class', () => {
  //   component.connexionformValidation();
  //   fixture.detectChanges();
  //   const userCurrent = new InfoUser();
  //   userCurrent.login = 'test';
  //   userCurrent.password = 'test';
  //   userCurrent.token = '+65ze+f6zf!6g4r65sf!mg65g4e6rgs156';
  //   userCurrent.message = 'lorem';
  //   userCurrent.nom = 'Doe';
  //   userCurrent.prenom = 'John';
  //   userCurrent.email = 'Dojohn-doe@gmail.fre';
  //   userCurrent.metier = 'Developpeur';
  //   userCurrent.age = 30;
  //   userCurrent.pays = 'France';
  //   userCurrent.description = 'Lorem ipsum tatanium Opano';
  //   userCurrent.avatar = 'logologin.jpg';
  //
  //   expect(userCurrent.nom).toHaveClass('test');
  // });


});
