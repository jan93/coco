import {Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {InfoUser} from '../shared/classes/info-user';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  connexionForm = new FormGroup({});
  afficheSpinner: boolean;
  hiddenLoginCard: boolean;

  constructor(private fb: FormBuilder, private router: Router, private infoUser: InfoUser) {
  }

  ngOnInit(): void {
    this.connexionForm = this.fb.group({
      login: ['test', Validators.required],
      password: ['test', Validators.required],
    });
    this.afficheSpinner = false; // initialisation
  }

  connexionformValidation(): void {
    const log = this.connexionForm.value.login;
    const pass = this.connexionForm.value.password;

    if (log === 'test' && pass === 'test') {

      this.infoUser.login = log;
      this.infoUser.password = pass;
      this.infoUser.token = '+65ze+f6zf!6g4r65sf!mg65g4e6rgs156';
      this.infoUser.message = 'lorem';
      this.infoUser.nom = 'Doe';
      this.infoUser.prenom = 'John';
      this.infoUser.email = 'Dojohn-doe@gmail.fre';
      this.infoUser.metier = 'Developpeur';
      this.infoUser.age = 30;
      this.infoUser.pays = 'France';
      this.infoUser.description = 'Lorem ipsum tatanium Opano';
      this.infoUser.avatar = 'logologin.jpg';

      console.log(this.infoUser);

      setTimeout(() => {
        this.afficheSpinner = true;
        sessionStorage.setItem('userConnected', this.infoUser.login);
        this.router.navigate(['layout/content']);
      }, 1000);
    } else {
      if (log !== 'test') {
        Swal.fire({
          title: 'Error!',
          text: 'il y\'a un problème dans le login',
          icon: 'warning',
          confirmButtonText: 'Cool',
        });
      } else if (pass !== 'test') {
        Swal.fire({
          title: 'Error!',
          text: 'il y\'a un problème dans le password',
          icon: 'warning',
          confirmButtonText: 'Cool',
        });
      } else {
        Swal.fire({
          title: 'Error!',
          text: 'il y\'a un problème dans le formulaire',
          icon: 'warning',
          confirmButtonText: 'Cool',
        });
      }
    }
  }


}
